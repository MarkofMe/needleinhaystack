open System.IO

[<EntryPoint>]
let main args = 

    let str = File.ReadAllLines(args.[0])
    let str2 = File.ReadAllLines(args.[1])
    
    let result = Set.intersect (set str) (set str2)

    result |> Seq.iter(fun x -> printfn "%A" x) 
    0